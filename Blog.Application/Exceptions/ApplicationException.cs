﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Application.Exceptions
{
    public class ApplicationException : Exception
    {
        public string Message { get; }
        public ApplicationException(string message) : base(message)
        {
            Message = message;
        }
    }
}
