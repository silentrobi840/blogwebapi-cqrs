﻿using Autofac;
using Blog.Application.Interfaces;
using Blog.Application.Services;

namespace Blog.Application
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
        }
    }
}
