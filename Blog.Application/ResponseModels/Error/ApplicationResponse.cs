﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Application.ResponseModels.Error
{
    public class ApplicationResponse
    {
        public string Message { get; set; }
    }
}
