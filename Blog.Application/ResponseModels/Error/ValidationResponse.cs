﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Application.ErrorModels
{
    public class ValidationResponse
    {
        public List<string> Errors { get; set; }
    }
}
