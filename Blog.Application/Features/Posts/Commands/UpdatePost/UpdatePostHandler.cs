using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Blog.Domain.Posts;
using Blog.Domain.Repositories;
using MediatR;

namespace Blog.Application.Features.Posts.Commands.UpdatePost
{
    public class UpdatePostHandler : IRequestHandler<UpdatePostCommand, bool>
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public UpdatePostHandler(IBlogUnitOfWork blogUnitOfWork, IMapper mapper)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Handle(UpdatePostCommand request, CancellationToken cancellationToken)
        {
            Post entity = _blogUnitOfWork.PostReadOnlyRepository.Get(request.Id);

            if (entity == null) throw new Exceptions.ApplicationException("No Post is found to update");

            entity.UpdatedAt = DateTime.Now;
            entity.Uploads = request.UpdatePostDto.Uploads == null || request.UpdatePostDto.Uploads.Count == 0 ? entity.Uploads : request.UpdatePostDto.Uploads;

            var isSucceed = _blogUnitOfWork.PostCommandRepository.Update(request.Id, entity);

            if (!isSucceed) throw new Exceptions.ApplicationException("Post is not updated successfully");

            return true;
        }
    }
}
