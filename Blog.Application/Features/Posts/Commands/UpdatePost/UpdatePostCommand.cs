using MediatR;

namespace Blog.Application.Features.Posts.Commands.UpdatePost
{
    public class UpdatePostCommand : IRequest<bool>
    {
            public UpdatePostDto UpdatePostDto { get; set; }

            public string Id { get; set; }
    }
}