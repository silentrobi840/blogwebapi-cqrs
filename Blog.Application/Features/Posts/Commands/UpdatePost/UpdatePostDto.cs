﻿using AutoMapper;
using Blog.Domain.Posts;
using System.Collections.Generic;

namespace Blog.Application.Features.Posts.Commands.UpdatePost
{
    [AutoMap(typeof(Post), ReverseMap = true)]
    public class UpdatePostDto
    {
        public string Content { get; set; }

        public List<string> Uploads { get; set; }
    }
}
