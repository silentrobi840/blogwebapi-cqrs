﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Application.Features.Posts.Commands.DeletePost
{
    public class DeletePostCommand : IRequest<bool>
    {
        public string Id { get; set; }
    }
}
