﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Application.Features.Posts.Commands.DeletePost
{
    public class DeletePostValidator : AbstractValidator<DeletePostCommand>
    {
        public DeletePostValidator()
        {
            RuleFor(model => model.Id)
                .Length(24).WithMessage("Id length should be 24");
        }
    }
}
