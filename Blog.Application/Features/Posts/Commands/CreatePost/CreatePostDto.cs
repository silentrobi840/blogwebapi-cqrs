﻿using AutoMapper;
using Blog.Domain.Posts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blog.Application.Features.Posts.Commands.CreatePost
{
    [AutoMap(typeof(Post), ReverseMap = true)]
    public class CreatePostDto
    {
        public string Content { get; set; }

        public List<string> Uploads { get; set; }

        public string UserId { get; set; }
    }
}