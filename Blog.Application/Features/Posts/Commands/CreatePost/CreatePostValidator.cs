﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Application.Features.Posts.Commands.CreatePost
{
    public class CreatePostValidator : AbstractValidator<CreatePostCommand>
    {
        public CreatePostValidator()
        {
            RuleFor(model => model.CreatePostDto.UserId)
              .Cascade(CascadeMode.Stop)
              .NotNull().WithMessage("Please ensure you have entered 'UserId' field")
              .Length(24).WithMessage("UserId length should be 24");

            RuleFor(model => model.CreatePostDto.Content)
              .Cascade(CascadeMode.Stop)
              .NotNull().WithMessage("Please ensure you have entered 'Content' field")
              .NotEmpty().WithMessage("Content field shouldn't be empty");

        }
    }
}
