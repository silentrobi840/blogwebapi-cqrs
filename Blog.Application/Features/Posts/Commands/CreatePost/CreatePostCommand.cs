using MediatR;

namespace Blog.Application.Features.Posts.Commands.CreatePost
{
    public class CreatePostCommand : IRequest<bool>
    {
        public CreatePostDto CreatePostDto { get; set; }
    }
}
