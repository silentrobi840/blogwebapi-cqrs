﻿using AutoMapper;
using Blog.Domain.Posts;
using Blog.Domain.Repositories;
using Blog.Domain.Users;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Application.Features.Posts.Commands.UnlikePost
{
    public class UnlikePostHandler : IRequestHandler<UnlikePostCommand,bool>
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public UnlikePostHandler(IBlogUnitOfWork blogUnitOfWork, IMapper mapper)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }
        public async Task<bool> Handle(UnlikePostCommand request, CancellationToken cancellationToken)
        {
            User userEntity = _blogUnitOfWork.UserReadOnlyRepository.Get(request.UserId);

            if (userEntity == null)
            {
                throw new Exceptions.ApplicationException("Failed to unlike: User Id is not valid");
            }

            Post postEntity = _blogUnitOfWork.PostReadOnlyRepository.Get(request.Id);

            if (postEntity == null) throw new Exceptions.ApplicationException("No Post is found to unlike");

            if (postEntity.LikedUsers.Count == 0 || !postEntity.LikedUsers.Contains(request.UserId)) 
            {
                throw new Exceptions.ApplicationException("Failed to perform operation as no like is found with given User Id");
            }

            postEntity.LikedUsers.Remove(request.UserId);

            var isSucceed = _blogUnitOfWork.PostCommandRepository.Update(request.Id, postEntity);

            if (!isSucceed) throw new Exceptions.ApplicationException("Like is not removed successfully");

            return true;
        }
    }
}
