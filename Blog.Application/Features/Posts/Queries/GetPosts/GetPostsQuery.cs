﻿using Blog.Application.Features.Posts.Queries.SeedWork;
using Blog.Domain.QueryMapper;
using MediatR;
using System.Collections.Generic;

namespace Blog.Application.Features.Posts.Queries.GetPosts
{
    public class GetPostsQuery : IRequest<IEnumerable<PostDto>>
    {
        public QueryOptions QueryOptions { get; set; }
    }
}
