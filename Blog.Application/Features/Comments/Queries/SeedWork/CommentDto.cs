using System.Collections.Generic;
using AutoMapper;
using Blog.Domain.Comments;

namespace Blog.Application.Features.Comments.Queries.SeedWork
{
    [AutoMap(typeof(Comment), ReverseMap = true)]
    public class CommentDto
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public List<string> LikedUsers { get; set; }

        [IgnoreMap]
        public int Likes => LikedUsers.Count;

        public string ParentCommentId { get; set; }
    }
}