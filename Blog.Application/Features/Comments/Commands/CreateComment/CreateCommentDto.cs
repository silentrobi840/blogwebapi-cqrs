﻿using AutoMapper;
using Blog.Domain.Comments;

namespace Blog.Application.Features.Comments.Commands.CreateComment
{
    [AutoMap(typeof(Comment), ReverseMap = true)]
    public class CreateCommentDto
    {
        public string Content { get; set; }

        public string ParentCommentId { get; set; }

        public string UserId { get; set; }
    }
}
