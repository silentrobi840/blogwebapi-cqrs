﻿using MediatR;

namespace Blog.Application.Features.Comments.Commands.UpdateComment
{
    public class UpdateCommentCommand : IRequest<bool>
    {
        public UpdateCommentDto UpdateCommentDto { get; set; }
        public string Id { get; set; }
        public string PostId { get; set; }

    }
}
