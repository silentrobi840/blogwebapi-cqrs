﻿using AutoMapper;
using Blog.Domain.Comments;
using Blog.Domain.Repositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Application.Features.Comments.Commands.UpdateComment
{
    public class UpdateCommentHandler : IRequestHandler<UpdateCommentCommand, bool>
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public UpdateCommentHandler(IBlogUnitOfWork blogUnitOfWork, IMapper mapper)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Handle(UpdateCommentCommand request, CancellationToken cancellationToken)
        {
            Comment entity = _blogUnitOfWork.CommentReadOnlyRepository.Get(request.PostId, request.Id);

            if (entity == null) return false;

            entity.Content = request.UpdateCommentDto.Content;
            entity.UpdatedAt = DateTime.Now;

            var isSucceed = _blogUnitOfWork.CommentCommandRepository.Update(request.PostId, request.Id, entity);

            if (!isSucceed) throw new Exceptions.ApplicationException("Failed to update comment");

            return true;
        }
    }
}
