﻿using AutoMapper;
using Blog.Domain.Comments;

namespace Blog.Application.Features.Comments.Commands.UpdateComment
{
    [AutoMap(typeof(Comment), ReverseMap = true)]
    public class UpdateCommentDto
    {
        public string Content { get; set; }
    }
}
