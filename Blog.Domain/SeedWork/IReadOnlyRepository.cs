﻿using Blog.Domain.QueryMapper;
using System.Collections.Generic;

namespace Blog.Domain.SeedWork
{
    public interface IReadOnlyRepository<T, TEntity>
    {
        IEnumerable<TEntity> GetMultiple(QueryOptions options);

        TEntity Get(T id);
    }
}
