using Blog.Domain.SeedWork;

namespace Blog.Domain.Posts
{
    public interface IPostReadOnlyRepository: IReadOnlyRepository<string, Post>
    {
  
    }
}
