using Blog.Domain.SeedWork;
using System.Collections.Generic;
using Blog.Domain.Comments;

namespace Blog.Domain.Posts
{
    public class Post : BaseEntity
    {
        public string Content { get; set; }

        public List<string> Uploads { get; set; }

        public List<string> LikedUsers { get; set; }

        public List<Comment> Comments { get; set; }

        public string UserId { get; set; }
    }
}