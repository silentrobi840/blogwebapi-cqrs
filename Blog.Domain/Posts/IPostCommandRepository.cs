﻿using Blog.Domain.SeedWork;

namespace Blog.Domain.Posts
{
    public interface IPostCommandRepository : ICommandRepository<string, Post>
    {
    }
}
