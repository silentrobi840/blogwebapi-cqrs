using Blog.Domain.SeedWork;

namespace Blog.Domain.Users 
{
    public interface IUserCommandRepository : ICommandRepository<string, User>
    {
        
    }
}