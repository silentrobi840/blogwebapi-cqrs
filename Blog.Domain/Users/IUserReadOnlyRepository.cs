﻿using Blog.Domain.SeedWork;

namespace Blog.Domain.Users 
{
    public interface IUserReadOnlyRepository : IReadOnlyRepository<string, User>
    {

    }
}
