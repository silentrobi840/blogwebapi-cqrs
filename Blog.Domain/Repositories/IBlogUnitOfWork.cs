using Blog.Domain.Comments;
using Blog.Domain.Posts;
using Blog.Domain.Users;

namespace Blog.Domain.Repositories
{
    public interface IBlogUnitOfWork
    {
        IPostReadOnlyRepository PostReadOnlyRepository { get; }
        IPostCommandRepository PostCommandRepository { get; }
        ICommentReadOnlyRepository CommentReadOnlyRepository { get; }
        ICommentCommandRepository CommentCommandRepository { get; }
        IUserCommandRepository UserCommandRepository { get; }
        IUserReadOnlyRepository UserReadOnlyRepository { get; }
    }
}