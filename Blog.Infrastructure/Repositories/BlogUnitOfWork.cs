using Blog.Domain.Comments;
using Blog.Domain.Posts;
using Blog.Domain.Repositories;
using Blog.Domain.Users;

namespace Blog.Infrastructure.Repositories
{
    public class BlogUnitOfWork : IBlogUnitOfWork
    {
        public BlogUnitOfWork(
        IPostReadOnlyRepository postReadOnlyRepository,
        IPostCommandRepository postCommandRepository,
        ICommentReadOnlyRepository commentReadOnlyRepository,
        ICommentCommandRepository commentCommandRepository,
        IUserCommandRepository userCommandRepository,
        IUserReadOnlyRepository userReadOnlyRepository)
        {
            PostReadOnlyRepository = postReadOnlyRepository;
            PostCommandRepository = postCommandRepository;
            CommentReadOnlyRepository = commentReadOnlyRepository;
            CommentCommandRepository = commentCommandRepository;
            UserReadOnlyRepository = userReadOnlyRepository;
            UserCommandRepository = userCommandRepository;
        }

        public IPostReadOnlyRepository PostReadOnlyRepository { get; }

        public IPostCommandRepository PostCommandRepository { get; }

        public ICommentReadOnlyRepository CommentReadOnlyRepository { get; }

        public ICommentCommandRepository CommentCommandRepository { get; }

        public IUserReadOnlyRepository UserReadOnlyRepository { get; }

        public IUserCommandRepository UserCommandRepository { get; }

    }
}