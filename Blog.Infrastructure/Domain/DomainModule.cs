﻿using Autofac;
using Blog.Domain.Comments;
using Blog.Domain.Posts;
using Blog.Domain.Repositories;
using Blog.Domain.Users;
using Blog.Infrastructure.Domain.Comments;
using Blog.Infrastructure.Domain.Posts;
using Blog.Infrastructure.Domain.Users;
using Blog.Infrastructure.Repositories;

namespace Blog.Domain
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BlogUnitOfWork>().As<IBlogUnitOfWork>().InstancePerLifetimeScope();

            builder.RegisterType<PostReadOnlyRepository>().As<IPostReadOnlyRepository>().InstancePerLifetimeScope();

            builder.RegisterType<PostCommandRepository>().As<IPostCommandRepository>().InstancePerLifetimeScope();

            builder.RegisterType<CommentReadOnlyRepository>().As<ICommentReadOnlyRepository>().InstancePerLifetimeScope();

            builder.RegisterType<CommentCommandRepository>().As<ICommentCommandRepository>().InstancePerLifetimeScope();

            builder.RegisterType<UserReadOnlyRepository>().As<IUserReadOnlyRepository>().InstancePerLifetimeScope();

            builder.RegisterType<UserCommandRepository>().As<IUserCommandRepository>().InstancePerLifetimeScope();
        }
    }
}
